package datadispatchers;

import org.java_websocket.WebSocket;

public interface DataDispatcher {
	public abstract void send(byte[] data, WebSocket connection);

	public abstract void send(String data, WebSocket connection);

}
