package datadispatchers;

import org.java_websocket.WebSocket;

import utilis.SnappyCompressUtil;

public class CompressedDataDispatcher implements DataDispatcher {

	@Override
	public void send(byte[] data, WebSocket connection) {
		byte[] compressed = SnappyCompressUtil.compress(data);
		connection.send(compressed);
	}

	@Override
	public void send(String data, WebSocket connection) {
		connection.send(data);
	}

}
