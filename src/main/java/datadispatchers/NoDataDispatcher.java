package datadispatchers;

import org.java_websocket.WebSocket;

public class NoDataDispatcher implements DataDispatcher {

	@Override
	public void send(byte[] data, WebSocket connection) {

	}

	@Override
	public void send(String data, WebSocket connection) {
		connection.send(data);
	}

}
