package datadispatchers;

import org.java_websocket.WebSocket;


public class StandardDataDispatcher implements DataDispatcher{

	@Override
	public void send(byte[] data, WebSocket connection) {
		connection.send(data);
	}

	@Override
	public void send(String data, WebSocket connection) {
		connection.send(data);
	}

}
