package audio;

import utilis.AverageUtil;

public class SilenceRecognizer {

	public double[] getSamples(byte[] sound) {
		double[] result = new double[sound.length / 2];
		int index = 0;
		for (int i = 0; i < result.length; i += 2) {
			result[index] = getSample(sound[i], sound[i + 1]);
			index++;
		}
		return result;
	}

	public float getSample(byte first, byte second) {
		int result = 0;
		result |= second & 0xFF;
		result |= first << 8;
		float sample = (result << 16) >> 16;

		return sample / 32764;

	}

	public boolean isVoice(byte[] sound, float tolerance) {
		double[] samples = getSamples(sound);
		double root = AverageUtil.rootMeanSquare(samples);
		System.out.println(root);
		return root > tolerance;
	}

}
