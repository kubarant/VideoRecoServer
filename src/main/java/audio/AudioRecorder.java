package audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import main.Config;

public class AudioRecorder {

	private float rmsTolerance;

	private TargetDataLine dataLine;
	private onAudioChangeListener onAudioChangeListener;
	private AudioFormat format;
	private SilenceRecognizer silenceRecognizer;
	private boolean unmuted = true;

	public AudioRecorder() {
		Config conf = Config.getInstance();
		rmsTolerance = Integer.parseInt(conf.getValue("RMS"));
		onAudioChangeListener = sound -> {
		};
		init();
		silenceRecognizer = new SilenceRecognizer();

	}

	public interface onAudioChangeListener {
		public void onAudioChange(byte[] sound);
	}

	public void record() throws Exception {
		new Thread(() -> {
			
			byte[] buffer = new byte[(int) (format.getSampleRate() * format.getFrameSize()/4)];
			System.out.println(dataLine.getBufferSize()+"   vs   "+ buffer.length);
			while (unmuted) {
				dataLine.read(buffer, 0, buffer.length);
				if (silenceRecognizer.isVoice(buffer, rmsTolerance))
					onAudioChangeListener.onAudioChange(buffer);
			}
			close();
		}).start();
	}

	private void init() {
		format = new AudioFormatFactory().createAudioFormat();
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
		try {
			dataLine = (TargetDataLine) AudioSystem.getLine(info);
			dataLine.open(format);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		dataLine.start();
	}

	public void close() {
		unmuted = false;
		dataLine.drain();
		dataLine.close();
	}

	public onAudioChangeListener getOnAudioChangeListener() {
		return onAudioChangeListener;
	}

	public void setOnAudioChangeListener(onAudioChangeListener onAudioChangeListener) {
		this.onAudioChangeListener = onAudioChangeListener;
	}
}
