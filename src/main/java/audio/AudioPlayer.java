package audio;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.pmw.tinylog.Logger;

import main.Config;

public class AudioPlayer {

	private SourceDataLine sourceDataLine;

	public AudioPlayer() {
		init();
	}

	public void init() {
		AudioFormat format = new AudioFormatFactory().createAudioFormat();
		Config config =Config.getInstance(); 
		int mixerNum = Integer.parseInt(config.getValue("mixer"));
		// For Windows
		/*
		 * DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class,
		 * format); sourceDataLine = (SourceDataLine)
		 * AudioSystem.getLine(dataLineInfo);
		 */

		// For Raspberry
		try {
			sourceDataLine = AudioSystem.getSourceDataLine(format, AudioSystem.getMixerInfo()[mixerNum]);
			sourceDataLine.open(format);
		} catch (LineUnavailableException e) {
			Logger.error(e);
			throw new RuntimeException(e);
		}
		FloatControl volumeControl = (FloatControl) sourceDataLine.getControl(FloatControl.Type.MASTER_GAIN);

		volumeControl.setValue(volumeControl.getMaximum());
		sourceDataLine.start();

	}

	public void playByteSound(byte[] sound) {

		System.out.println("Playing  " + sound);
		sourceDataLine.write(sound, 0, sound.length);

	}

	public void close() {
		sourceDataLine.drain();
		sourceDataLine.close();

	}

}
