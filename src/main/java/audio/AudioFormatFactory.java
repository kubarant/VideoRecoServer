package audio;
import javax.sound.sampled.AudioFormat;

public class AudioFormatFactory {
	
	public AudioFormat createAudioFormat(){
		float sampleRate= 16000;
        int sampleSizeInBits=16;
        int channels=1;
        boolean signed=true;
        boolean bigEndian=false;
        
		AudioFormat format=	new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
		return format;

	}

}
