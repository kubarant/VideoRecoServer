package main;

import java.net.InetSocketAddress;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.pmw.tinylog.Logger;

import models.User;
import models.UserList;

public abstract class WebServer extends WebSocketServer {
	private UserList users;

	public WebServer(InetSocketAddress address) {
		super(address);
		users = new UserList();
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		Logger.info("New connection from   " + conn.getLocalSocketAddress());
		users.add(conn);
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		Logger.info(
				"Disconnection of " + conn.getLocalSocketAddress() + " because of " + reason + " with code " + code);
		users.removeConnection(conn);
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		Logger.error("Error throwed \n" + ex);
		users.removeConnection(conn);
	}

	protected void closeAllConnections(int code) {
		for (User user : users) {
			users.removeConnection(user.getConnection());
			user.getConnection().close(code);
		}
	}

	public UserList getUsers() {
		return users;
	}

	@Override
	public void stop() {
		try {
			super.stop();
		} catch (Exception e) {
			Logger.error(e);

		}

	}
}