package main;

import java.util.Scanner;

/**
 * Created by Kuba on 2017-04-14.
 */

public class Server {

	public static void main(String[] args) throws Exception {
		WebSocketMediaServer as = new WebSocketMediaServer();
		as.start();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			as.close();
			System.out.println("Bye ");
		}));

		// new Thread(new FaceRecognizerThread()).start();
		Scanner scan = new Scanner(System.in);
		while (!((scan.nextLine()).equals("quit"))) {
		}
		scan.close();
	}

}
