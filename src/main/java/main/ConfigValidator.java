package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.pmw.tinylog.Logger;

public class ConfigValidator {

	public boolean isValid(String path, Set<Object> required) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(path));
		} catch (IOException e) {
			Logger.error(e);
			return false;
		}
		return containsKeysAndValues(required, properties);
	}

	private boolean containsKeysAndValues(Set<Object> keySet, Properties properties) {
		for (Object key : keySet) {
			if ((!properties.containsKey(key)) && (properties.get(key) == null)) {
				Logger.error("Invalid key: "+key.toString()+" in configuration file");
				return false;
			}
		}
		return true;
	}
	

}
