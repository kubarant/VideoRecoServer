package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.pmw.tinylog.Logger;

public class ConfigLoader {

	private ConfigValidator configValidator;

	public ConfigLoader() {
		configValidator = new ConfigValidator();
	}

	/**
	 * @param configPath
	 *            path to configuration file
	 * @return loaded properties or default if any problems
	 */
	public Properties load(String configPath) {
		try {
			return tryLoad(configPath);
		} catch (IOException e) {
			Logger.error("Problem with loading properties using defaults");
		}
		return createDefaultProperties();
	}

	/**
	 * 
	 * @param configPath
	 *            path to configuration file
	 * @return properties load from configPath or default if any error
	 */
	private Properties tryLoad(String configPath) throws IOException {
		Properties defaul = createDefaultProperties();
		if (configValidator.isValid(configPath, defaul.keySet())) {
			Properties loaded = new Properties();
			loaded.load(new FileInputStream(configPath));
			return loaded;
		} else {
			defaul.store(new FileOutputStream(new File(configPath)), "");

			return defaul;
		}
	}

	private Properties createDefaultProperties() {
		Properties defaultConfig = new Properties();
		defaultConfig.setProperty("log_location", System.getProperty("user.dir") + File.separator);
		defaultConfig.setProperty("mixer", "4");
		defaultConfig.setProperty("RMS", "0.35");
		return defaultConfig;
	}
}
