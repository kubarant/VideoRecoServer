package main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tcphandlers.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Kuba on 2017-04-14.
 */
public class ExtendedServerSocket {
    private Socket socket;
    private ArrayList<ConnectionHandler> handlers=new ArrayList<>();
    private Logger logger=LoggerFactory.getLogger(ExtendedServerSocket.class);
    private BufferedReader bufferedReader;

    public ExtendedServerSocket(Socket socket) throws IOException {
        this.socket=socket;
        initHandlers();

        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

    }
    public void initHandlers(){
        handlers.add(new NullHandler("null",socket));
        handlers.add(new EchoHandler("echo",socket));
        handlers.add(new QuitHandler("quit",socket));
        handlers.add(new ImageDownloadHandler("accepted",socket));
        handlers.add(new NewPersonAdderHandler("new",socket));
        handlers.add(new VideoStreamHandler("stream",socket));

    }

    public String socketCommand() throws IOException {

        while(bufferedReader.read()==-1){}
        String result=bufferedReader.readLine();

        return Optional.ofNullable(result).orElse("null");
    }

    public void delegateConnection() throws IOException {
        String command =socketCommand();
        logger.info("Socket revive "+command+" command");
        for (ConnectionHandler handler:handlers) {
            if(handler.getCommandName().equals(command)){
                handler.execute();
                return;
            }
        }

    }






}
