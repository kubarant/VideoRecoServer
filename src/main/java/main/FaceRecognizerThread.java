package main;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.pmw.tinylog.Logger;



/**
 * Created by Kuba on 2017-05-07.
 */
public class FaceRecognizerThread implements Runnable {
	private Scanner scanner;

	private static PrintWriter writer;

	@Override
	public void run() {
		Logger.info("Launch recognition script");
		Runtime runtime = Runtime.getRuntime();
		try {
			Process process = runtime.exec("sudo nice -n -19 python3 /home/pi/Desktop/FaceRec.py");

			scanner = new Scanner(process.getInputStream());

			writer = new PrintWriter(process.getOutputStream(), true);
			while (true)
				catchScriptLogs();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void redirectLogs(String msg) {
		Logger.info(msg);
	}

	private void catchScriptLogs() {
		String msg = scanner.nextLine();
		if (msg.contains("LOG"))
			redirectLogs(msg);
		else;
	}

	public static synchronized void openGate() {
		if (writer != null) {
			writer.println("open");
		}
	}

}
