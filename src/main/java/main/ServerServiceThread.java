package main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Kuba on 2017-04-14.
 */
public class ServerServiceThread implements Runnable {
    private ExtendedServerSocket socket;
    private Logger logger= LoggerFactory.getLogger(ServerServiceThread.class);

    public ServerServiceThread(Socket socket) throws IOException {
    this.socket=new ExtendedServerSocket(socket);
    logger.info("New connection from: "+String.valueOf(socket.getInetAddress()));

    }

    public void run() {

            while (!Thread.interrupted()) {
                try {
                    logger.info("New connection established");
                    socket.delegateConnection();
                }
                catch (IOException e) {
                        e.printStackTrace();
                    }


            }




    }
}
