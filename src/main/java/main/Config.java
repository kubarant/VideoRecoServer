package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
	private final String userDir = System.getProperty("user.home");
	private final String configName = "config.xyz";
	private final String configPath = userDir + File.separator + configName;
	private Properties config;

	private ConfigLoader loader;

	private static class ConfigHolder {
		private static final Config CONFIGURATION = new Config();
	}

	private Config() {
		loader = new ConfigLoader();
		config = loader.load(configPath);
	}

	public static Config getInstance() {
		return ConfigHolder.CONFIGURATION;
	}

	public String getValue(String key) {
		return config.getProperty(key);
	}

	public void setValue(String key, String value) {
		config.setProperty(key, value);
	}

	public void reload() {
		close();
		config = loader.load(configPath);
	}

	public void close() {
		try {
			config.store(new FileOutputStream(new File(configPath)), "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}