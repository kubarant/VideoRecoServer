package main;

import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.pmw.tinylog.writers.FileWriter;

public class LoggerConfiguration {
	public static final String LOG_FILE="logs.txt";
	
	private final String LOG_FORMAT ="{date}   {Thread} {class_name}.{method}()  \n {level}	 {message} ";
	
	public void configure(){
	
		ConsoleWriter stdio = new ConsoleWriter();
		FileWriter file = new FileWriter(LOG_FILE);
		Configurator.currentConfig().removeAllWriters().addWriter(stdio).addWriter(file).formatPattern(LOG_FORMAT).activate();
	}
	

}
