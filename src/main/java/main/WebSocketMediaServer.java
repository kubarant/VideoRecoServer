package main;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.pmw.tinylog.Logger;

import audio.AudioPlayer;
import senders.AudioSender;
import senders.VideoSender;
import webhandlers.OpenGateWebHandler;
import webhandlers.RegisterConnectionWebHandler;
import webhandlers.UserPreferencesWebHandler;
import webhandlers.WebsocketHandler;

/**
 * Created by Kuba on 2017-05-08.
 */
public class WebSocketMediaServer extends WebServer {
	private final int SERVER_CLOSE_CODE = 225;
	private AudioPlayer player;
	private ArrayList<WebsocketHandler> handlers;
	private AudioSender audioSender;
	private VideoSender videoSender;

	public WebSocketMediaServer() throws Exception {
		super(new InetSocketAddress("192.168.88.200", 9822));
		handlers = createHandlers();
		audioSender = new AudioSender(getUsers());
		videoSender = new VideoSender(getUsers());
		Thread audioSend = new Thread(audioSender);
		Thread videoSend = new Thread(videoSender);
		audioSend.start();
		videoSend.start();

		player = new AudioPlayer();

	}

	@Override
	public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
		super.onOpen(webSocket, clientHandshake);
	}

	public void onClose(WebSocket webSocket, int i, String s, boolean b) {
		super.onClose(webSocket, i, s, b);
	}

	public void onMessage(WebSocket webSocket, String s) {
		Logger.info("Recived message: " + s);
		for (WebsocketHandler handler : handlers) {
			if (handler.isValid(s))
				handler.execute(webSocket, s);
		}
	}

	@Override
	public void onMessage(WebSocket conn, ByteBuffer message) {
		super.onMessage(conn, message);
		System.out.println(message.array().length);
		byte[] messageB = message.array();
		player.playByteSound(messageB);

	}

	public void onError(WebSocket webSocket, Exception e) {
		super.onError(webSocket, e);

	}

	public ArrayList<WebsocketHandler> createHandlers() {
		ArrayList<WebsocketHandler> handlers = new ArrayList<WebsocketHandler>();
		handlers.add(new OpenGateWebHandler());
		handlers.add(new RegisterConnectionWebHandler(getUsers()));
		handlers.add(new UserPreferencesWebHandler(getUsers()));
		return handlers;
	}

	public void close() {
		super.closeAllConnections(SERVER_CLOSE_CODE);
		audioSender.close();
		videoSender.close();
		player.close();
		stop();
	}

}
