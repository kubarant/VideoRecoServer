package models;

import datadispatchers.CompressedDataDispatcher;
import datadispatchers.DataDispatcher;
import datadispatchers.NoDataDispatcher;
import datadispatchers.StandardDataDispatcher;

public enum UserType {
	Mobile(true, true,new CompressedDataDispatcher()), Browser(true, false,new StandardDataDispatcher()), Unknown(false, false, new NoDataDispatcher());

	private boolean canVideo;
	private boolean canAudio;
	private DataDispatcher dispatcher;
	
	UserType(boolean canVideo, boolean canAudio, DataDispatcher dispatcher) {
		this.canAudio = canAudio;
		this.canVideo = canVideo;
		this.dispatcher = dispatcher;
	}

	public boolean canVideo() {
		return canVideo;
	}

	public void setCanVideo(boolean canVideo) {
		this.canVideo = canVideo;
	}

	public boolean canAudio() {
		return canAudio;
	}

	public void setCanAudio(boolean canAudio) {
		this.canAudio = canAudio;
	}

	public DataDispatcher getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(DataDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}
	
}
