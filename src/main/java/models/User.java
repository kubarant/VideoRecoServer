package models;

import org.java_websocket.WebSocket;

public class User {
	private String name;
	private WebSocket connection;
	private UserType userType;

	public User(String name, WebSocket connection, UserType userType) {
		this.name = name;
		this.connection = connection;
		this.userType = userType;
	}

	public User(WebSocket connection) {
		super();
		this.connection = connection;
		this.userType = UserType.Unknown;
		this.name = "Not Introduced Yourself";
	}

	public User() {
	}

	public void send(byte[] data) {
		userType.getDispatcher().send(data, connection);
	}

	public void send(String data) {
		userType.getDispatcher().send(data, connection);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WebSocket getConnection() {
		return connection;
	}

	public void setConnection(WebSocket connection) {
		this.connection = connection;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", connection=" + connection + ", userType=" + userType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((connection == null) ? 0 : connection.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((userType == null) ? 0 : userType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (connection == null) {
			if (other.connection != null)
				return false;
		} else if (!connection.equals(other.connection))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (userType != other.userType)
			return false;
		return true;
	}

}
