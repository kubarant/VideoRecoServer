package models;

import java.util.concurrent.CopyOnWriteArrayList;

import org.java_websocket.WebSocket;

public class UserList extends CopyOnWriteArrayList<User> {
	private static final long serialVersionUID = 1L;

	public User findUserByWebsocket(WebSocket websocket) {
		for (User user : this) {
			if (user.getConnection().equals(websocket))
				return user;
		}
		return new User();
	}

	public void add(WebSocket webSocket) {
		User user = new User(webSocket);
		add(user);
	}

	public void removeConnection(WebSocket webSocket) {
		User user=findUserByWebsocket(webSocket);
		if(user != null)
			remove(user);
	}
	
}
