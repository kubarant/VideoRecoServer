package senders;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Iterator;

import org.pmw.tinylog.Logger;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.v4l4j.V4l4jDriver;

import models.User;
import models.UserList;
import utilis.ByteArrayCombinatorUtil;
import utilis.ImageUtilis;

public class VideoSender implements Runnable {
	private static final int VIDEO_DATA_ID = 1;
	private Webcam webcam;
	private UserList userList;
	private boolean run = true;

	public VideoSender(UserList userList) {
		this.userList = userList;
		Webcam.setDriver(new V4l4jDriver());
		webcam = Webcam.getDefault();
		
		webcam.setViewSize(new Dimension(640, 480));
		webcam.open();

	}

	public void sendWebcamFrameToUsers() {
		byte[] imageBytes = getClassifiedImageData();
		Iterator<User> iterator = userList.iterator();
		while (iterator.hasNext()) {
			Logger.info("sending " + imageBytes.length);
			User user = iterator.next();
			sendFrameToUser(user, imageBytes);
		}
	}

	public void sendFrameToUser(User user, byte[] image) {
		boolean canVideo = user.getUserType().canVideo();
		if (canVideo)
			user.send(image);
	}

	private byte[] getClassifiedImageData() {
		BufferedImage img = webcam.getImage();
		byte[] imageBytes = ImageUtilis.BufferedImageToByteArray(img);
		return img == null ? new byte[0]:ByteArrayCombinatorUtil.combineTwo(new byte[] { VIDEO_DATA_ID }, imageBytes);
	}

	public void run() {
		while (run) {
			try {
				Thread.sleep(12);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			sendWebcamFrameToUsers();
		}
	}

	public void close() {
		run = false;
		webcam.close();
	}

}
