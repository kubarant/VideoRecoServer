package senders;

import java.util.Iterator;

import org.pmw.tinylog.Logger;

import audio.AudioRecorder;
import models.User;
import models.UserList;
import utilis.ByteArrayCombinatorUtil;

public class AudioSender implements Runnable {
	private static final byte AUDIO_DATA_ID = 2;
	private UserList users;
	private AudioRecorder recorder;
	private boolean run = true;

	public AudioSender(UserList users) {
		this.users = users;
		recorder = new AudioRecorder();

	}

	public void sendSoundToUsers(byte[] sound) {
		byte[] audio = classifyAudio(sound);
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			Logger.info("Audio send " + audio.length);
			User user = iterator.next();
			sendSoundToUser(user, audio);
		}
	}

	public void sendSoundToUser(User user, byte[] sound) {
		boolean canAudio = user.getUserType().canAudio();
		if (canAudio)
			user.send(sound);
	}

	private byte[] classifyAudio(byte[] sound) {
		return ByteArrayCombinatorUtil.combineTwo(new byte[] { AUDIO_DATA_ID }, sound);

	}

	public void run() {
		try {
			recorder.record();
			recorder.setOnAudioChangeListener(e -> {
				if (run)
					sendSoundToUsers(e);
				else
					recorder.close();
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void close() {
		recorder.close();
	}

}
