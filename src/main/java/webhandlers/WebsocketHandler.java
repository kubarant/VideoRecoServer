package webhandlers;

import org.java_websocket.WebSocket;

public abstract class WebsocketHandler {


	public WebsocketHandler() {}

	/**
	 * It checks is @param command valid for this handler Default implementation
	 * check is @param command equal to handler getCommand method
	 * @param command  message from server
	 * @return is message directed to this handler
	 */
	public boolean isValid(String command) {
		return command.equalsIgnoreCase(getCommand());
	}

	/**
	 * @return identifier for command
	 */
	public abstract String getCommand();
	
	
	public abstract void execute(WebSocket websocket,String command);

}
