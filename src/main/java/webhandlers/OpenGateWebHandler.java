package webhandlers;

import org.java_websocket.WebSocket;
import org.pmw.tinylog.Logger;

import main.FaceRecognizerThread;

public class OpenGateWebHandler extends WebsocketHandler {

	@Override
	public String getCommand() {
		return "Open";
	}

	@Override
	public void execute(WebSocket websocket, String command) {
		Logger.info("Opening a gate");
		FaceRecognizerThread.openGate();
	}

}
