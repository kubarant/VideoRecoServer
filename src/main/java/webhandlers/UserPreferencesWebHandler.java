package webhandlers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.java_websocket.WebSocket;
import org.pmw.tinylog.Logger;

import models.User;
import models.UserList;

public class UserPreferencesWebHandler extends WebsocketHandler {
	private final String regex = "set\\s(canVideo|canAudio)\\s(true|false)";
	private Pattern pattern;
	private UserList list;

	public UserPreferencesWebHandler(UserList list) {
		this.list = list;
		pattern = Pattern.compile(regex);
	}

	@Override
	public boolean isValid(String command) {
		Matcher matcher = pattern.matcher(command);
		return matcher.matches();
	}

	@Override
	public String getCommand() {
		return "preferences";
	}

	@Override
	public void execute(WebSocket websocket, String command) {
		Matcher matcher = pattern.matcher(command);
		matcher.find();
		String permission = matcher.group(1);
		boolean state = Boolean.valueOf(matcher.group(2));
		User user = list.findUserByWebsocket(websocket);
		changePermission(user, permission, state);
		Logger.info("Change "+permission+" permission on state "+state+" for user named "+user.getName());
	}

	private void changePermission(User user, String permission, boolean state) {
		if (permission.equals("canVideo")) {
			user.getUserType().setCanVideo(state);
		}
		if (permission.equals("canAudio")) {
			user.getUserType().setCanAudio(state);
		}

	}

}
