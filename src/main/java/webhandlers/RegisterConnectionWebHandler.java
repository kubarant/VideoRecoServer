package webhandlers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.java_websocket.WebSocket;
import org.pmw.tinylog.Logger;

import models.User;
import models.UserList;
import models.UserType;

public class RegisterConnectionWebHandler extends WebsocketHandler {

	private String pattern = "^name\\s{1}(Mobile|Browser|Unknown)\\s{1}(.{3,95})\\z";

	private UserList users;

	public RegisterConnectionWebHandler(UserList users) {
		this.users = users;

	}

	@Override
	public boolean isValid(String command) {
		return command.matches(pattern);
	}

	@Override
	public String getCommand() {
		return "register";
	}

	@Override
	public void execute(WebSocket websocket, String command) {
		Pattern patt = Pattern.compile(pattern);
		Matcher matcher = patt.matcher(command);
		matcher.find();
		String type = matcher.group(1);
		String name = matcher.group(2);
		UserType userType = UserType.valueOf(type);
		Logger.info("Users " + users);
		users.removeConnection(websocket);
		users.add(new User(name, websocket, userType));
	}

}
