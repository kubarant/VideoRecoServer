package utilis;

public class AverageUtil {
	
	public static double average(double[] given) {
		double sum = 0.0f;
		for (int i = 0; i < given.length; i++) {
			sum += given[i];
		}
		return sum / given.length;
	}

	public static double rootMeanSquare(double[] given) {
		double sum = 0;
		double avrg = average(given);
		for (int i = 0; i < given.length; i++) {
			sum += Math.pow(given[i] - avrg, 2d);
		}
		double rsm = Math.sqrt(sum / given.length);
		return rsm;
	}
}
