package utilis;

public class ByteArrayCombinatorUtil {
	
	/*
	 * It combining first and second byte arrays
	 */
	public static byte[] combineTwo(byte[] first, byte[] second){
		byte[] result= new byte[first.length+second.length];
		
		for (int i = 0; i < first.length; i++) {result[i]=first[i];}
		
		for (int i = first.length; i < result.length; i++) {result[i]=second[i-first.length];}
		
		return result;
	}
	
	/* It combining all byte arrays given as var args
	 * 
	 */
	public static byte[] combine(byte[]... arrays){
		byte[] result = new byte[0];
		for (int i = 0; i < arrays.length; i++) {
			result= combineTwo(result, arrays[i]);
		}
			
			return result;
		}
		
	
	

	

}
