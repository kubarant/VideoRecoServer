package utilis;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

/**
 * Created by Kuba on 2017-04-21.
 * It's compatible with FileDownloaderUtil
 *
 *  it send to client number of files then it send name of file, size of file in bytes, then it sends array of bytes
 */

public class FileUploaderUtil {
    private DataOutputStream writer;
    public FileUploaderUtil(OutputStream out){
        writer=new DataOutputStream(out);
    }

    /**
     *
     * @param name name of sended file
     * @param content content of sended file
     * @throws IOException
     *
     *
     */
    public  void sendFile(String name, byte[] content) throws IOException {
        writer.writeUTF(name);
        writer.writeUTF(String.valueOf(content.length));
        writer.write(content);


    }

    /**
     *
     * @param fileMap it hashmap with name of file as key and content of file as byte array
     * @param numberOfFiles it's number of files that should be send
     * @throws IOException
     */
    public void sendFiles(HashMap<String, byte[]> fileMap, int numberOfFiles) throws IOException {
        writer.writeUTF(String.valueOf(numberOfFiles));
        for (String key : fileMap.keySet()) {
            sendFile(key,fileMap.get(key));
        }

    }
}
