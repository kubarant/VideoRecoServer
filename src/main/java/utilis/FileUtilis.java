package utilis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

/**
 * Created by Kuba on 2017-04-18.
 */
public class FileUtilis {

    public static File[] getFilesFrom(String path){
        return new File(path).listFiles();
    }

    /**
     *@param path path to directory
     * @return return HashMap with keys as file names and bytes as their content
     */
    public static HashMap<String,byte[]> makeFilesContentMap(String path){
        File[] files = getFilesFrom(path);
        HashMap<String, byte[]> result= new HashMap<>();
        for (int i = 0; i < files.length; i++) {
            result.put(files[i].getName(),getBytesFromFile(files[i]));
        }
        return result;

    }
    public static byte[] getBytesFromFile(File file){
       try {
         byte[] bytes=Files.readAllBytes(file.toPath());
         return bytes;
        } catch (IOException e) {
            e.printStackTrace();

        }
        return new byte[]{};
    }

    public static Byte[] byteArrToByteArr(byte[] byteArr){
        Byte[] bytes = new Byte[byteArr.length];
        for (int i = 0; i < byteArr.length; i++) {
            bytes[i]=new Byte(byteArr[i]);
        }
        return bytes;
    }

    public static File bytesToFile(String path, String name, byte[] content){
        try {
            FileOutputStream fileOut = new FileOutputStream(path+"\\"+name);
            fileOut.write(content);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new File(path+"\\"+name);
    }


}
