package utilis;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;

/**
 * Created by Kuba on 2017-04-21.
 */
public class ImageUtilis {

public static byte[] BufferedImageToByteArray(BufferedImage img){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	try {
//		ImageIO.write(img, "jpg", baos);
		createImage(baos,img);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	byte[] bytes = baos.toByteArray();
	return bytes;
}
private static void createImage(ByteArrayOutputStream baos, BufferedImage image) throws IOException{
	ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
	writer.setOutput(ImageIO.createImageOutputStream(baos));
	JPEGImageWriteParam param = new JPEGImageWriteParam(Locale.getDefault());
	param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	param.setCompressionQuality(0.5f);
	writer.write(null, new IIOImage(image, null, null), param);

}

}
