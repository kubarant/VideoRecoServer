package utilis;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Kuba on 2017-05-07.
 */
public class VideoStreamThread implements Runnable {
    private static final String CURRENT_FRAME_PATH = "/home/pi/Desktop/frame.jpg";
    private static final String FRAME_CATCH_COMMAND="fswebcam -r 720x1280 --no-banner ";
    private final FileUploaderUtil uploader;
    private  Socket client;

    public VideoStreamThread(Socket client) throws IOException {
        this.client=client;
        uploader = new FileUploaderUtil(client.getOutputStream());

    }
    @Override
    public void run() {
        Runtime runtime = Runtime.getRuntime();
        while (true){
            try {
                Process frameCatcherCommand = runtime.exec(FRAME_CATCH_COMMAND+CURRENT_FRAME_PATH);
                frameCatcherCommand.waitFor();
                byte[] currentFrameContent = FileUtilis.getBytesFromFile(new File(CURRENT_FRAME_PATH));
                uploader.sendFile("frame", currentFrameContent);
                Thread.sleep(28);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }
}
