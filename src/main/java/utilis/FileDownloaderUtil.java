package utilis;


import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Kuba on 2017-04-21.
 *
 * It's compatible with FileUploaderUtil
 * First it get file name, then lenght of file, then content as byte[]
 */

public class FileDownloaderUtil  {

    private final DataInputStream dataInputStream;
    private final String path;

    public FileDownloaderUtil(InputStream input, String path) {
        this.path = path;
        dataInputStream = new DataInputStream(input);
    }
    public File[] downloadFiles() throws IOException {
        int numberOfFiles = Integer.parseInt(dataInputStream.readUTF());
        File[] files = new File[numberOfFiles];
        for (int i = 0; i < numberOfFiles; i++) {
            files[i]=downloadFile();


        }
    return  files;
    }

    public File downloadFile() throws IOException {
        String fileName = dataInputStream.readUTF();
        int fileLenght = Integer.parseInt(dataInputStream.readUTF());
        byte[] bytes = new byte[fileLenght];
        dataInputStream.read(bytes);
        return FileUtilis.bytesToFile(path,fileName,bytes);

    }


}
