package utilis;

import java.io.IOException;

import org.pmw.tinylog.Logger;
import org.xerial.snappy.Snappy;

/**
 * It just little wrapper for Snappy compress methods which handle exceptions
 * 
 *
 */
public class SnappyCompressUtil {
	public static byte[] compress(byte[] data) {
		try {
			Logger.info("Before "+data.length);
			byte[] result = Snappy.compress(data);
			Logger.info("After "+result.length);
			return result;
		} catch (IOException e) {
			Logger.error(e);
			return data;
		}

	}
}
