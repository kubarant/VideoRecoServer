package tcphandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utilis.FileDownloaderUtil;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Kuba on 2017-04-21.
 */
public class NewPersonAdderHandler extends ConnectionHandler {

    private final String commandName;
    private final Socket socket;
    private  FileDownloaderUtil downloader;
    private Logger logger = LoggerFactory.getLogger(NewPersonAdderHandler.class);

    public NewPersonAdderHandler(String commandName, Socket socket) {
        super(commandName, socket);
        this.commandName = commandName;
        this.socket = socket;

        try {
            downloader = new FileDownloaderUtil(socket.getInputStream(),"\\accessed\\acc\\");
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    public void execute() throws IOException {
        File file = downloader.downloadFile();
        logger.info("New person is added: "+file.getName());


    }
}
