package tcphandlers;

import utilis.VideoStreamThread;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Kuba on 2017-05-07.
 */
public class VideoStreamHandler extends  ConnectionHandler {

    public VideoStreamHandler(String commandName, Socket socket) {
        super(commandName, socket);


    }

    @Override
    public void execute() throws IOException {
    new Thread(new VideoStreamThread(socket)).start();
    }
}
