package tcphandlers;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Kuba on 2017-04-14.
 */
public abstract class ConnectionHandler {

    private final String commandName;
    Socket socket;

    public ConnectionHandler(String commandName, Socket socket){
            this.commandName=commandName;
            this.socket=socket;

    }

    public abstract void execute() throws IOException;

    public String getCommandName() {
        return commandName;
    }
}
