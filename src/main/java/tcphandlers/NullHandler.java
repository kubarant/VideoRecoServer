package tcphandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;

/**
 * Created by Kuba on 2017-04-14.
 */
public class NullHandler extends ConnectionHandler {
    Logger logger= LoggerFactory.getLogger(NullHandler.class);

    public NullHandler(String commandName, Socket socket) {
        super(commandName, socket);
    }

    @Override
    public void execute() {
        logger.error("Null handler handle something");

    }
}
