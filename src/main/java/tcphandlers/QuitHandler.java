package tcphandlers;

import java.io.IOException;
import java.net.Socket;

import tcphandlers.ConnectionHandler;

/**
 * Created by Kuba on 2017-04-14.
 */
public class QuitHandler extends ConnectionHandler {

    private final String commandName;
    private final Socket socket;

    public QuitHandler(String commandName, Socket socket) {
        super(commandName, socket);
        this.commandName = commandName;
        this.socket = socket;
    }

    @Override
    public void execute() {

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread.currentThread().interrupt();
    }
}
