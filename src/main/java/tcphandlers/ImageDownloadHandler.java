package tcphandlers;

import utilis.FileUploaderUtil;
import utilis.FileUtilis;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by Kuba on 2017-04-18.
 */

/**
    When it get request, first it send to client number of files then it send name of file, size of file in bytes, then it sends array of bytes
 **/
public class ImageDownloadHandler extends ConnectionHandler {

    private final String commandName;
    private final Socket socket;
    private  DataOutputStream writer;
    private FileUploaderUtil fileUploader;

    public ImageDownloadHandler(String commandName, Socket socket)  {
        super(commandName, socket);
        this.commandName = commandName;
        this.socket = socket;

        try {

            fileUploader = new FileUploaderUtil(socket.getOutputStream());
            writer = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    @Override
    public void execute() {
        HashMap<String, byte[]> fileMap = FileUtilis.makeFilesContentMap("\\accepted");
        int numberOfFiles = fileMap.keySet().size();
        try {
            fileUploader.sendFiles(fileMap,numberOfFiles);

        } catch (IOException e)
        {e.printStackTrace();}
    }






    @Override
    public String getCommandName() {
        return commandName;
    }
}
