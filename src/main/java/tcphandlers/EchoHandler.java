package tcphandlers;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Kuba on 2017-04-14.
 */
public class EchoHandler extends ConnectionHandler{

    private final String commandName;
    private final Socket socket;

    public EchoHandler(String commandName, Socket socket) {
        super(commandName, socket);
        this.commandName = commandName;
        this.socket = socket;
    }

    @Override
    public void execute() {

        PrintWriter printWriter=null;
        try {
            printWriter = new PrintWriter(socket.getOutputStream(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        printWriter.println("echo");
    }
}
