import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.java_websocket.WebSocket;
import org.junit.Before;
import org.junit.Test;

import models.User;
import models.UserList;
import models.UserType;
import webhandlers.UserPreferencesWebHandler;

public class UserPreferencesHandlerTest {
	UserPreferencesWebHandler userPreferencesWebHandler;
	UserList list;

	@Before
	public void setUp() {
		list = new UserList();
		userPreferencesWebHandler = new UserPreferencesWebHandler(list);
	}

	@Test
	public void testPositiveValidation() {
		String good = "set canVideo false";
		boolean real = userPreferencesWebHandler.isValid(good);
		assertTrue("It should be correct form", real);

	}

	@Test
	public void testNegativeValidation() {
		String bad = "set canVideo False";
		boolean real = userPreferencesWebHandler.isValid(bad);
		assertFalse("It shouldn't be correct form", real);

		bad = "set canSound false";
		real = userPreferencesWebHandler.isValid(bad);
		assertFalse("It shouldn't be correct form", real);

		bad = "sets canVideo False";
		real = userPreferencesWebHandler.isValid(bad);
		assertFalse("It shouldn't be correct form", real);

	}
	@Test
	public void testPositiveExecute() {
		String good = "set canVideo false";
		User user =new User();
		WebSocket web =mock(WebSocket.class);
		
		user.setConnection(web);
		user.setUserType(UserType.Unknown);
		list.add(user);
		
		userPreferencesWebHandler.execute(web, good);
		boolean test = list.get(0).getUserType().canVideo() == false;
		assertTrue(test);
	}


}
