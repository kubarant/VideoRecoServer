import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import utilis.ByteArrayCombinatorUtil;

public class ByteCombinatorTest {

	@Test
	public void testCombine(){
		byte[] first = {1,2,3,4};
		byte[] second = {5,6,7,8};
		byte[] third = {9,10,11,12};
		
		byte[] expected = {1,2,3,4,5,6,7,8,9,10,11,12};
		byte[] actuals = ByteArrayCombinatorUtil.combine(first, second,third);
		
		assertArrayEquals(expected, actuals);
		
	}
	
	@Test
	public void testCombineTwo() {
		byte[] first = {1,2,3,4};
		byte[] second = {5,6,7,8};
		
		byte[] expected = {1,2,3,4,5,6,7,8};
		byte[] actuals = ByteArrayCombinatorUtil.combineTwo(first, second);
		
		assertArrayEquals(expected, actuals);
		
		
	}
	
}
