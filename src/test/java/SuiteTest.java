import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ByteCombinatorTest.class, RegisterHandlerTest.class, UserPreferencesHandlerTest.class })
public class SuiteTest {

}
