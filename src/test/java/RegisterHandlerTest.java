import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import models.User;
import models.UserList;
import models.UserType;
import webhandlers.RegisterConnectionWebHandler;

public class RegisterHandlerTest {

	private RegisterConnectionWebHandler registerHandler;
	private User expected;
	private UserList users;

	@Before
	public void setUp() {
		users = new UserList();
		registerHandler = new RegisterConnectionWebHandler(users);
		expected = new User();
		expected.setConnection(null);
		expected.setName("Samsung gtx 970 AM3+");
		expected.setUserType(UserType.Mobile);
	}

	@Test
	public void testPositiveValidator() {
		String command = "name Mobile Samsung-gt 900p";
		boolean good = registerHandler.isValid(command);
		assertTrue("Given string should match for register", good);
	}

	@Test
	public void testNegativeValidator() {
		String command = "name Mobile as";
		boolean bad = registerHandler.isValid(command);
		assertFalse("Given string shouldn't match too short name", bad);

		command = "name Broswer ziemiak";
		bad = registerHandler.isValid(command);
		assertFalse("Given string shouldn't match invalid user type", bad);

		command = "names  Mobile asass";
		bad = registerHandler.isValid(command);
		assertFalse("Given string shouldn't match for white spaces", bad);
	}

	@Test
	public void testExecutePositive() {
		registerHandler.execute(null,"name Mobile Samsung gtx 970 AM3+");
		User real = users.get(0);
		assertTrue("It should be same", real.equals(expected));

	}

	@Test
	public void testExecuteNegative() {
		registerHandler.execute(null,"name Mobile ziemna");
		User real = users.get(0);
		real.setName("notexpected");

		assertFalse("He has other name", real.equals(expected));
	}

}
