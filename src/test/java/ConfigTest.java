import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import main.Config;

public class ConfigTest {

	private Config config;

	@Before
	public void setup() {
		config = Config.getInstance();
	}

	@Test
	public void test() {
		String excepted = "Test";

		config.setValue("TestKey", "Test");

		assertEquals(excepted, config.getValue("TestKey"));
		config.reload();

		assertEquals(excepted, config.getValue("TestKey"));

	}

}
