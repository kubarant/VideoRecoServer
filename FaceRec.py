# Standard output is redirected to server i/o streams

import face_recognition
import _thread
import os
import re
from subprocess import call


known_people_encodings =[]
known_people_names=[]
opened = False
works = True
known_people_path=os.path.join(os.path.expanduser("~"),"people" )

def open_gate():
    global opened
    if(opened == False):

        opened = True



def communicate():
    while True:
        command=input()
        if(command == "open"):
            open_gate()


def image_files_in_folder(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]


def scan_known_people(known_people_folder):
    known_names = []
    known_face_encodings = []

    for file in image_files_in_folder(known_people_folder):
        basename = os.path.splitext(os.path.basename(file))[0]
        img = face_recognition.load_image_file(file)
        encodings = face_recognition.face_encodings(img)

        known_names.append(basename)
        known_face_encodings.append(encodings[0])

    return known_names, known_face_encodings


def has_permissions(param):
    pass


def is_known(unknown_encoding):
    result=face_recognition.compare_faces(known_people_encodings,unknown_encoding)

    for i in range(0,len(result)):
        if(result[i]):
         return True
         





(known_people_names,known_people_encodings) =scan_known_people(known_people_path)

_thread.start_new_thread(communicate(),"Communication thread")

while works:
     switch_state = False
     if(switch_state):
         call("fswebcam -r 640x480 --no-banner temp.jpg")

         unknow_image= face_recognition.load_image_file("~/temp.jpg")
         unknow_face_location=face_recognition.face_locations(unknow_image)
         unknow_encoded = face_recognition.face_encodings(unknow_image,unknow_face_location)

         if(is_known(unknow_encoded)):
             open_gate()


